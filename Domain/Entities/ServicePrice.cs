﻿namespace DentalClinic.Business.Domain.Entities
{
    /// <summary>
    /// Услуга с ценой
    /// </summary>
    public class ServicePrice : BaseEntity
    {
        /// <summary>
        /// Задает, возвращает код обслуживания
        /// </summary>
        public string? Code { get; set; }

        /// <summary>
        /// Задает, возвращает название услуги
        /// </summary>
        public string? Name { get; set; }

        /// <summary>
        /// Задает, возвращает цену услуги
        /// </summary>
        public decimal Price { get; set; }        
    }
}
