﻿namespace DentalClinic.Business.Domain.Entities
{
    /// <summary>
    /// Запись к врачу онлайн
    /// </summary>
    public class OnlineAppointment : BaseEntity
    {
        /// <summary>
        /// Задает, возвращает номер телефона
        /// </summary>
        public string? PhoneNumber { get; set; }

        /// <summary>
        /// Задает, возвращает почту
        /// </summary>
        public string? Email { get; set; }

        /// <summary>
        /// Задает, возвращает желамую дату посещения
        /// </summary>
        public DateTime? WantedDateVisit { get; set; }

        /// <summary>
        /// Задает, возвращает описание
        /// </summary>
        public string? Description { get; set; }

        /// <summary>
        /// Задает, возвращает значение, указывающее, было ли запись одобрена
        /// </summary>
        public bool Approved { get; set; }
    }
}
