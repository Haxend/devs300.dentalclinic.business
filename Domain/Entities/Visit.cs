﻿using DentalClinic.Business.Domain.Enum;

namespace DentalClinic.Business.Domain.Entities
{
    /// <summary>
    /// Класс посещения клиники
    /// </summary>
    public class Visit : BaseEntity
    {
        /// <summary>
        /// Задает, возвращает дату и время начала
        /// </summary>
        public DateTime? DateTimeBegin { get; set; }

        /// <summary>
        /// Задает, возвращает дату и время конца
        /// </summary>
        public DateTime? DateTimeEnd { get; set; }

        /// <summary>
        /// Возвращает, задает дату посещения
        /// </summary>
        public DateTime? DateOfVisit { get; set; }

        /// <summary>
        /// Возвращает, задает доктора
        /// </summary>
        public Guid? DoctorID { get; set; }
                       
        /// <summary>
        /// Возвращает, задает пациента
        /// </summary>
        public Guid? PatientID { get; set; }


        /// <summary>
        /// Возвращает, задает статус
        /// </summary>
        public StatusOfVisit? Status { get; set; }

        /// <summary>
        /// Возвращает, задает список комментариев
        /// </summary>
        public string? Comment { get; set; }

        /// <summary>
        /// Возвращает, задает значение, указывающее, было ли посещение онлайн
        /// </summary>
        public bool FromOnline { get; set; }

        #region "References"

        public virtual List<VisitProcedure> Procedures { get; set; } = new List<VisitProcedure>();

        /// <summary>
        /// Возвращает, задает посещение доктора
        /// </summary>
      //  public virtual VisitToDoctor? VisitTreatment { get; set; }

        #endregion
    }
}
