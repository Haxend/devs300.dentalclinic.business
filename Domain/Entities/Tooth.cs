﻿namespace DentalClinic.Business.Domain.Entities
{
    public class Tooth : BaseEntity
    {
        public string Name { get; set; }

        public int Code { get; set; }
    }
}
