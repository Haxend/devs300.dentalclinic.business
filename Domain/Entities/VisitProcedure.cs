﻿namespace DentalClinic.Business.Domain.Entities
{
    public class VisitProcedure : BaseEntity
    {
        /// <summary>
        /// Оказанная при визите услуга
        /// </summary>
        public virtual ServicePrice Service { get; set; }

        /// <summary>
        /// Количество данных услуг (н-р, услуга "чистка канала", 4 канала в зубе, значит, 4 чистки канала)
        /// </summary>
        public int CountOfThisProcedures { get; set; }

        /// <summary>
        /// Итоговая сумма услуги. Т.к. прайс может меняться, надо зафиксировать сумму
        /// </summary>
        public decimal Sum {  get; set; }

        /// <summary>
        /// Ссылка на визит
        /// </summary>             
        public virtual Visit Visit { get; set; }

        /// <summary>
        /// Зуб
        /// </summary>
        public virtual Tooth? Tooth { get; set; }
    }
}
