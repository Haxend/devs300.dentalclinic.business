﻿
namespace DentalClinic.Business.Domain.Enum
{
    /// <summary>
    /// Статус визита
    /// </summary>
    public enum StatusOfVisit
    {
        ///<summary>Не указан</summary>
        None = 0,

        ///<summary>Зарегистрирован</summary>
        Registered = 1,

        ///<summary>Завершён</summary>
        Success = 2,

        ///<summary>Перенесен</summary>
        Rescheduled = 3,

        ///<summary>Отменен</summary>
        Canceled = 4
    }
}
