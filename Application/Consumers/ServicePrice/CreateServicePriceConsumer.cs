﻿using AutoMapper;
using DentalClinic.Auth.Application.Interfaces;
using DentalClinic.Auth.Application.Services;
using DentalClinic.Common.Contracts.Doctor;
using DentalClinic.Common.Exceptions;
using MassTransit;

namespace DentalClinic.Auth.Application.Consumers
{
    public class CreateServicePriceConsumer : IConsumer<DoctorCreateRequest>
    {
        private readonly IUserRepository<Domain.Doctor> _doctorRepository;
        private readonly PasswordGeneration _passwordGeneration;
        private readonly IMapper _mapper;

        public CreateServicePriceConsumer(IUserRepository<Domain.Doctor> doctorRepository,
                                    IMapper mapper)
        {
            _doctorRepository = doctorRepository;
            _mapper = mapper;
        }

        public async Task Consume(ConsumeContext<DoctorCreateRequest> context)
        {
            var request = context.Message;

            if (await _doctorRepository.HasAnyByPhoneAsync(request.PhoneNumber))
                throw new ConflictException("Пользователь с таким номером телефона уже существует!");

            var doctor = _mapper.Map<Domain.Doctor>(request);

            doctor.Id = Guid.NewGuid();
            var pass = _passwordGeneration.GeneratePasswordAndHash();
            doctor.PasswordHash = pass.Item2;
            doctor.RoleId = Guid.Parse("60e26b53-f985-4cd8-ad5f-3f75a56368ce");

            var id = await _doctorRepository.CreateAsync(doctor);

            // todo: Отправляем уведомление через DentalClinic.Notification
            //await context.Publish(new UserCreatedNotification
            //{
            //    Email = request.Email,
            //    FirstName = request.FirstName,
            //    LastName = request.LastName,
            //});

            await context.RespondAsync(_mapper.Map<DoctorCreateResponse>(doctor));
        }
    }
}
