﻿using DentalClinic.Business.Domain;

namespace DentalClinic.Business.Application.Interfaces
{
    public interface IUnitOfWork
    {
        IBaseRepository<T> GetRepository<T>() where T : BaseEntity;
        int SaveChanges();
        Task<int> SaveChangesAsync();
    }
}
