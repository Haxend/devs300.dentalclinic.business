﻿using DentalClinic.Business.Domain;

namespace DentalClinic.Business.Application.Interfaces;

public interface IBaseRepository<T> where T : BaseEntity
{
    Task<Guid> CreateAsync(T entity);
    Task UpdateAsync(T entity);
    Task DeleteAsync(T entity);
    Task<IEnumerable<T>> GetAllAsync();
    Task<T?> GetByIdAsync(Guid id);
    Task<bool> HasAnyByIdAsync(Guid id);
}