﻿using DentalClinic.Application.Models.OnlineAppointment.Request;
using FluentValidation;

namespace DentalClinic.Application.Models.OnlineAppointment
{
    public class OnlineAppointmentValidator : AbstractValidator<OnlineAppointmentApproveRequest>
    {
        public OnlineAppointmentValidator()
        {
            RuleFor(r => r.FirstName).Must(r => !string.IsNullOrEmpty(r)).WithMessage("Поле должно быть заполнено");
            RuleFor(r => r.LastName).Must(r => !string.IsNullOrEmpty(r)).WithMessage("Поле должно быть заполнено");
            RuleFor(r => r.FatherName).Must(r => !string.IsNullOrEmpty(r)).WithMessage("Поле должно быть заполнено");
            RuleFor(r => r.VisitId).Must(r => r != Guid.Empty).WithMessage("Поле \"Доступные слоты\" должно быть заполнено");
        }
    }
}
