﻿namespace DentalClinic.Application.Models.OnlineAppointment.Request
{
    public class OnlineAppointmentUpdateRequest
    {
        public string? PhoneNumber { get; set; }

        public string? Email { get; set; }

        public DateTime? WantedDateVisit { get; set; }

        public string? Description { get; set; }

        public bool Approved { get; set; }
    }
}
