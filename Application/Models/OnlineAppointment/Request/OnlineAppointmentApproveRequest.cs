﻿namespace DentalClinic.Application.Models.OnlineAppointment.Request
{
    public class OnlineAppointmentApproveRequest
    {
        public Guid VisitId { get; set; }

        public string FirstName { get; set; }

        public string FatherName { get; set; }

        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        public string Comment { get; set; }
    }
}
