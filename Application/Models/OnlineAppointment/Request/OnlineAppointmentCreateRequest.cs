﻿namespace DentalClinic.Application.Models.OnlineAppointment.Request
{
    public class OnlineAppointmentCreateRequest
    {
        public string? PhoneNumber { get; set; }

        public string? Email { get; set; }

        public DateTime? WantedDateVisit { get; set; }

        public string? Description { get; set; }
    }
}
