﻿using DentalClinic.Application.Models.Base;

namespace DentalClinic.Application.Models.OnlineAppointment.Response
{
    public class OnlineAppointmentApproveResponse : BaseResponse
    {
        public Guid VisitId { get; set; }
    }
}
