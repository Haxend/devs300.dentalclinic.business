﻿using DentalClinic.Application.Models.Base;

namespace DentalClinic.Application.Models.OnlineAppointment.Response
{
    public class OnlineAppointmentUpdateResponse : BaseResponse
    {
        public Guid Id { get; set; }
    }
}
