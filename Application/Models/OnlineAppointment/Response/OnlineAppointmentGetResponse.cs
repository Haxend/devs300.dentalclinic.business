﻿using DentalClinic.Application.Models.Base;

namespace DentalClinic.Application.Models.OnlineAppointment.Response
{
    public class OnlineAppointmentGetResponse : BaseResponse
    {
        public Guid Id { get; set; }

        public OnlineAppointmentDto Data { get; set; }
    }
}
