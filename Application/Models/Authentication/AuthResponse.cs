﻿namespace DentalClinic.Application.Models.Authentication;

public class AuthResponse
{
    public bool IsAuthenticated { get; set; }
    public string AccessToken { get; set; }
    public string RefreshToken { get; set; }
    public string Roles { get; set; }
    public string PersonId { get; set; }
    public string PersonFullName { get; set; }
}
