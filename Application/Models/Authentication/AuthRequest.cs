﻿namespace DentalClinic.Application.Models.Authentication;

public class AuthRequest
{
    public string Login { get; set; }

    public string Password { get; set; }
}
