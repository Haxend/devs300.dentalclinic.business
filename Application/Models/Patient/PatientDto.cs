﻿namespace DentalClinic.Application.Models.Patient.Dto
{

    public class PatientDto
    {
        public Guid Id { get; set; }
        /// <summary>
        /// Возвращает, задает имя пользователя
        /// </summary>
        public string? FirstName { get; set; }

        /// <summary>
        /// Возвращает, задает отчество пользователя
        /// </summary>
        public string? FatherName { get; set; }

        /// <summary>
        /// Возвращает, задает фамилию пользователя
        /// </summary>
        public string LastName { get; set; } = null!;

        /// <summary>
        /// Возвращает, задает почту пользователя
        /// </summary>
        public string? Email { get; set; }

        /// <summary>
        /// Возвращает, задает телефонный номер пользователя
        /// </summary>
        public string? PhoneNumber { get; set; }

        /// <summary>
        /// Возвращает, задает дату создания пользователя
        /// </summary>
        public DateTime? CreatedAt { get; set; }

        public string Allergy { get; set; } = null!;
    }
}
