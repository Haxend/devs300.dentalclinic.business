﻿using DentailClinic.Domain.Enum;

namespace DentalClinic.Application.Models.Patient.Dto
{
    public class PatientVisitsData
    {
        public string? FullNamePatient { get; set; }
        public string? Allergy { get; set; }

        public ICollection<VisitDoctorDto>? Visits { get; set; }
    }
}
