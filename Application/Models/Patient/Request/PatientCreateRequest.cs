﻿using System.ComponentModel.DataAnnotations;

namespace DentalClinic.Application.Models.Patient.Request
{
    public class PatientCreateRequest
    {
        public string? FirstName { get; set; }

        public string? FatherName { get; set; }

        public string LastName { get; set; } = null!;

        public string? Email { get; set; }

        public string? PhoneNumber { get; set; }

        public string Allergy { get; set; } = null!;
    }
}
