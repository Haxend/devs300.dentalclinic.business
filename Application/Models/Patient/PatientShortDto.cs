﻿namespace DentalClinic.Application.Models.Patient.Dto
{
    public class PatientShortDto
    {
        public Guid Id { get; set; }

        public string Description { get; set; }
    }
}
