﻿using DentalClinic.Application.Models.Base;
using DentalClinic.Application.Models.Patient.Dto;

namespace DentalClinic.Application.Models.Patient.Response
{
    public class PatientVisitsResponse : BaseResponse
    {
        public PatientVisitsData Data { get; set; }
    }
}
