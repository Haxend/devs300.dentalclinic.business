﻿using DentalClinic.Application.Models.Base;
using DentalClinic.Application.Models.Patient.Dto;

namespace DentalClinic.Application.Models.Patient.Response
{
    public class PatientGetResponse : BaseResponse
    {
        public Guid Id { get; set; }

        public PatientDto Data { get; set; }
    }
}
