﻿using DentalClinic.Application.Models.Base;

namespace DentalClinic.Application.Models.Patient.Response
{
    public class PatientCreateResponse : BaseResponse
    {
        public Guid Id { get; set; }
    }
}
