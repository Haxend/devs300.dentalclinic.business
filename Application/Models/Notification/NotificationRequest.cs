﻿
namespace DentalClinic.Application.Models.Notification;

public record NotificationRequest(string ToEmail, string Subject, string Body);
