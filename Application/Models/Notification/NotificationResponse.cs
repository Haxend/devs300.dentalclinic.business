﻿namespace DentalClinic.Application.Models.Notification;

public record NotificationResponse(bool Status, string message);
