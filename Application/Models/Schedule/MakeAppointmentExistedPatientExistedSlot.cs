﻿namespace DentalClinic.Application.Models.Schedule
{
    public class MakeAppointmentExistedPatientExistedSlot
    {
        public Guid PatientId { get; set; }
        public Guid VisitId { get; set; }
        public string Comment { get; set; }
        public Guid DoctorId { get; set; }
    }
}
