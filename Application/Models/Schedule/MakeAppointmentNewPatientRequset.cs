﻿namespace DentalClinic.Application.Models.Schedule
{
    public class MakeAppointmentNewPatientRequset
    {
        public string FirstName { get; set; }
        public string FatherName { get; set; }
        public string Surname { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public Guid VisitId { get; set; }
        public string Comment { get; set; }
    }
}
