﻿namespace DentalClinic.Application.Models.Schedule
{
    public class SchedulePeriodDto
    {
        public string PeriodName { get; set; }
        public List<OneDayScheduleDto> Days { get; set; }
    }

}
