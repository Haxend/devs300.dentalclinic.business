﻿namespace DentalClinic.Application.Models.Schedule
{
    public class OneDayScheduleDto
    {
        public string Date { get; set; }
        public List<VisitByDayDto> Visits { get; set; }
    }

}
