﻿
using DentalClinic.Application.Features.Doctors.Queries;
using DentalClinic.Application.Models.Patient.Dto;

namespace DentalClinic.Application.Models.Schedule
{
    public class ScheduleSlotDto
    {
        public DateTime? Begin {  get; set; }
        public DateTime? End { get; set; }
        public bool IsReserved { get; set; }
        public PatientDto? Patient { get; set; }
        public DoctorDto? Doctor { get; set; }
        public string? Comment { get; internal set; }
    }

}
