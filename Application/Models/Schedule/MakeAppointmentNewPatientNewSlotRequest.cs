﻿namespace DentalClinic.Application.Models.Schedule
{
    public class MakeAppointmentNewPatientNewSlotRequest
    {
        public string FirstName { get; set; }
        public string FatherName { get; set; }
        public string Surname { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public DateTime DateVisit { get; set; }
        public Guid? DoctorId { get; set; } = null!;
        public string Comment { get; set; }
    }
}
