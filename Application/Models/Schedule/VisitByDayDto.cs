﻿using DentailClinic.Domain.Enum;
using DentalClinic.Application.Services;

namespace DentalClinic.Application.Models.Schedule
{
    public class VisitByDayDto
    {
        public string TimePeriod { get; set; }
        public string Patient { get; set; }
        public string Doctor { get; set; }
        public string? Comment { get; set; }
        public string Status { get; private set; }
        public string VisitId { get; set; }

        internal static VisitByDayDto Create(DentailClinic.Domain.Entities.Visit visitByDay)
        {
            if (visitByDay.DateTimeEnd == null || visitByDay.DateTimeBegin == null)
                throw new Exception("У слота не задано начало или конец!");

            var visitByDayDto = new VisitByDayDto
            {
                TimePeriod = DatePeriodsService.GetSimplePeriod(visitByDay.DateTimeBegin.Value.ToLocalTime(), visitByDay.DateTimeEnd.Value.ToLocalTime())// visitByDay.DateTimeBegin.Value.ToString("HH:mm") + " - " + visitByDay.DateTimeEnd.Value.ToString("HH:mm")
            };

            if (visitByDay.Doctor != null)
                visitByDayDto.Doctor = visitByDay.Doctor.LastName + " " + visitByDay.Doctor.FirstName + " " + visitByDay.Doctor.FatherName;

            if (visitByDay.Patient != null)
                visitByDayDto.Patient = visitByDay.Patient.LastName + " " + visitByDay.Patient.FirstName + " " + visitByDay.Patient.FatherName;

            visitByDayDto.Comment = visitByDay.Comment;
            visitByDayDto.Status = visitByDay.Status==null?"-1":visitByDay.Status.Value.ToString();

            visitByDayDto.VisitId = visitByDay.Id.ToString();
            return visitByDayDto;
        }
    }

    public class VisitWithProceduresDto
    {
        public string TimePeriod { get; set; }
        public string Patient { get; set; }
        public string Doctor { get; set; }
        public string Status { get; set; }
        public string VisitId { get; set; }
    }

   

}
