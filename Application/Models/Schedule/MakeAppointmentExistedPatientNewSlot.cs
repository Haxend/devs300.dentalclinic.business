﻿namespace DentalClinic.Application.Models.Schedule
{
    public class MakeAppointmentExistedPatientNewSlot
    {
        public Guid PatientId { get; set; }
        public DateTime DateVisit { get; set; }
        public Guid? DoctorId { get; set; } = null!;
        public string Comment { get; set; }
    }
}
