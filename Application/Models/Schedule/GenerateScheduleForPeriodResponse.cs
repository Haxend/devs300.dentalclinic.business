﻿using DentalClinic.Application.Models.Base;

namespace DentalClinic.Application.Models.Schedule
{
    public class GenerateScheduleForPeriodResponse : BaseResponse
    {
        public List<ScheduleSlotDto> ScheduleSlots { get; set; } = new List<ScheduleSlotDto>();
    }
}
