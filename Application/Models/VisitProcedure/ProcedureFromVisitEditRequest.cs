﻿using DentalClinic.Application.Models.ServicePrice;
using DentalClinic.Application.Models.Tooth;

namespace DentalClinic.Application.Models.VisitProcedure
{
    public class ProcedureFromVisitEditRequest
    {
        // id процедуры
        public Guid Id { get; set; }

        public virtual ServicePriceEditRequest Service { get; set; }

        public int CountOfThisProcedures { get; set; }

        public decimal Sum { get; set; }

        public virtual ToothRequest Tooth { get; set; }
    }
}
