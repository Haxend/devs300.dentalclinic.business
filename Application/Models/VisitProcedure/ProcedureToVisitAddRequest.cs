﻿using DentalClinic.Application.Models.ServicePrice;
using DentalClinic.Application.Models.Tooth;

namespace DentalClinic.Application.Models.VisitProcedure
{
    public class ProcedureToVisitAddRequest
    {
        public virtual ServicePriceCreateRequest Service { get; set; }

        public int CountOfThisProcedures { get; set; }

        public decimal Sum { get; set; }

        public virtual ToothRequest Tooth { get; set; }
    }
}
