﻿using DentalClinic.Application.Models.ServicePrice;
using DentalClinic.Application.Models.Tooth;

namespace DentalClinic.Application.Models.VisitProcedure
{
    public class VisitProcedureShortDto
    {
        /// <summary>
        /// Возвращает, задает Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Оказанная при визите услуга
        /// </summary>
        public virtual ServicePriceDto Service { get; set; } = new();

        /// <summary>
        /// Количество данных услуг (н-р, услуга "чистка канала", 4 канала в зубе, значит, 4 чистки канала)
        /// </summary>
        public int CountOfThisProcedures { get; set; }

        /// <summary>
        /// Итоговая сумма услуги. Т.к. прайс может меняться, надо зафиксировать сумму
        /// </summary>
        public decimal Sum { get; set; }
        
        /// <summary>
        /// Зуб
        /// </summary>
        public virtual ToothDto? Tooth { get; set; }
    }
}
