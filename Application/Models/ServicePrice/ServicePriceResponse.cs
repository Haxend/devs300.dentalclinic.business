﻿namespace DentalClinic.Application.Models.ServicePrice
{
    public class ServicePriceResponse
    {
        public Guid Id { get; set; }

        public string? Code { get; set; }

        public string? Name { get; set; }

        public decimal Price { get; set; }
    }
}
