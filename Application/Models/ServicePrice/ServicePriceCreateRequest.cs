﻿namespace DentalClinic.Application.Models.ServicePrice
{
    public class ServicePriceCreateRequest
    {
        /// <summary>
        /// Задает, возвращает код обслуживания
        /// </summary>
        public string? Code { get; set; }

        /// <summary>
        /// Задает, возвращает название услуги
        /// </summary>
        public string? Name { get; set; }

        /// <summary>
        /// Задает, возвращает цену услуги
        /// </summary>
        public decimal Price { get; set; }
    }
}
