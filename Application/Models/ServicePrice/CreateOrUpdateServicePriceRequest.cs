﻿namespace DentalClinic.Application.Models.ServicePrice
{
    public class CreateOrUpdateServicePriceRequest
    {
        public string? Code { get; set; }

        public string? Name { get; set; }

        public decimal Price { get; set; }
    }
}
