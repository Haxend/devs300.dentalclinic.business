﻿namespace DentalClinic.Application.Models.ServicePrice
{
    public class ServicePriceDto
    {
        /// <summary>
        /// Возвращает, задает Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Задает, возвращает код обслуживания
        /// </summary>
        public string? Code { get; set; }

        /// <summary>
        /// Задает, возвращает название услуги
        /// </summary>
        public string? Name { get; set; }

        /// <summary>
        /// Задает, возвращает цену услуги
        /// </summary>
        public decimal Price { get; set; }
    }
}
