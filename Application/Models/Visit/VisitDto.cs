﻿
using DentailClinic.Domain.Enum;
using DentalClinic.Application.Features.Doctors.Queries;
using DentalClinic.Application.Models.Patient.Dto;
using DentalClinic.Application.Models.VisitProcedure;
using DentalClinic.Application.Services;

namespace DentalClinic.Application.Models
{

    /// <summary>
    /// Класс ДТО посещения
    /// </summary>
    public class VisitDto
    {
        /// <summary>
        /// Возвращает, задает id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Задает, возвращает дату и время начала
        /// </summary>
        public DateTime? DateTimeBegin { get; set; }

        /// <summary>
        /// Задает, возвращает дату и время конца
        /// </summary>
        public DateTime? DateTimeEnd { get; set; }

        /// <summary>
        /// Возвращает, задает дату посещения
        /// </summary>
        public DateTime? DateOfVisit { get; set; }

        public string PeriodSimple 
        { 
            get
            {
                if (DateTimeBegin == null) return string.Empty;
                if (DateTimeEnd == null) return string.Empty;
                return DatePeriodsService.GetSimplePeriod(DateTimeBegin.Value,DateTimeEnd.Value);
                //return DateTimeBegin.Value.ToLocalTime().ToString("HH:mm") + " - " + DateTimeEnd.Value.ToLocalTime().ToString("HH:mm");
            }
         }

        public string DateVisit
        {
            get
            {
                if (DateTimeBegin==null) return string.Empty;
                return DateTimeBegin.Value.ToLocalTime().ToString("dd.MM.yyyy");
            }
        }

        /// <summary>
        /// Возвращает, задает доктора
        /// </summary>
        public virtual DoctorDto? Doctor { get; set; }

        /// <summary>
        /// Возвращает, задает пациента
        /// </summary>
        public virtual PatientDto? Patient { get; set; }

        /// <summary>
        /// Возвращает, задает статус
        /// </summary>
        public StatusOfVisit? Status { get; set; }

        /// <summary>
        /// Возвращает, задает комментарий
        /// </summary>
        public string? Comment { get; set; }

        /// <summary>
        /// Возвращает, задает значение, указывающее, было ли посещение онлайн
        /// </summary>
        public bool FromOnline { get; set; }

        #region "References"

        public virtual List<VisitProcedureShortDto> Procedures { get; set; } = new List<VisitProcedureShortDto>();

        #endregion
    }

    public class AvailablePeriodDto
    {
        public string Period { get; set; }
    }
}
