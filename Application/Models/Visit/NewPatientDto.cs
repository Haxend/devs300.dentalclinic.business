﻿namespace DentalClinic.Application.Models
{
    public class NewPatientDto
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }         
    }
}
