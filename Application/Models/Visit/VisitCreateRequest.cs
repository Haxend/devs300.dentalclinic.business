﻿using DentailClinic.Domain.Enum;
using DentalClinic.Application.Features.Doctors.Queries;
using DentalClinic.Application.Models.Patient.Request;
using DentalClinic.Application.Models.VisitProcedure;

namespace DentalClinic.Application.Models
{
    public class VisitCreateRequest
    {
        /// <summary>
        /// Задает, возвращает дату и время начала
        /// </summary>
        public DateTime? DateTimeBegin { get; set; }

        /// <summary>
        /// Задает, возвращает дату и время конца
        /// </summary>
        public DateTime? DateTimeEnd { get; set; }

        /// <summary>
        /// Возвращает, задает дату посещения
        /// </summary>
        public DateTime? DateOfVisit { get; set; }

        /// <summary>
        /// Возвращает, задает доктора
        /// </summary>
        public virtual DoctorDto? Doctor { get; set; }

        /// <summary>
        /// Возвращает, задает пациента
        /// </summary>
        public virtual PatientCreateRequest? Patient { get; set; }

        /// <summary>
        /// Возвращает, задает статус
        /// </summary>
        public StatusOfVisit? Status { get; set; }

        /// <summary>
        /// Возвращает, задает комментарий
        /// </summary>
        public string? Comment { get; set; }

        /// <summary>
        /// Возвращает, задает значение, указывающее, было ли посещение онлайн
        /// </summary>
        public bool FromOnline { get; set; }

        #region "References"

        public virtual List<VisitProcedureCreateRequest> Procedures { get; set; } = new List<VisitProcedureCreateRequest>();

        #endregion
    }
}
