﻿
using System.ComponentModel.DataAnnotations;


namespace DentalClinic.Application.Models.Visit
{
    [AttributeUsage(AttributeTargets.Class)]
    public class VisitPatientRequireAttribute : ValidationAttribute
    {
        public VisitPatientRequireAttribute()
        {
            ErrorMessage = "Должен быть заполнен Id пациента или данные нового пациента!";
        }

        public override bool IsValid(object value)
        {            
            var visit = value as VisitSimpleDto;
            if (visit == null)
            {
                throw new NotSupportedException();
            }
            if (visit.PatientId==null &&
                (visit.NewPatient==null ||
                 string.IsNullOrEmpty(visit.NewPatient.LastName) ||
                 string.IsNullOrEmpty(visit.NewPatient.PhoneNumber))) 
            {
                return false;
            }

            return true;
        }
    }
}
