﻿
using FluentValidation;

namespace DentalClinic.Application.Models.Visit
{
    public class VisitValidator : AbstractValidator<VisitSimpleDto>
    {
        public VisitValidator()
        {
          
            RuleFor(p => p.Comment).MaximumLength(100).WithMessage("Слишком длинный комментарий!");
            //RuleFor(p => p.DoctorId).Must(p => p.HasValue && p!=Guid.Empty).WithMessage("Нужно указать врача!");
            RuleFor(p => p.NewPatient).SetValidator(new PatientValidator());

          
        }
    }

    public class PatientValidator : AbstractValidator<NewPatientDto>
    {
        public PatientValidator()
        {
            RuleFor(p => p.PhoneNumber).Matches(@"^[0-9]{10}$").WithMessage("Неверный формат номера телефона");
        }
    }
}
