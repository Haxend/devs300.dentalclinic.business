﻿namespace DentalClinic.Application.Models
{
    public class SlotTimeDto
    {
        public string SlotTime { get; set; }
        public Guid VisitId { get; set; }
    }
}
