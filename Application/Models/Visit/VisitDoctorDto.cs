﻿using DentalClinic.Application.Models.VisitProcedure;

namespace DentalClinic.Application.Models
{

    public class VisitDoctorDto
    {
        public string? FullNameDoctor { get; set; }
        public string? Comment { get; set; }
        public DateTime? DateOfVisit { get; set; }

        public virtual ICollection<VisitrProcedureDoctorDto>? Procedures { get; set; }
    }
}
