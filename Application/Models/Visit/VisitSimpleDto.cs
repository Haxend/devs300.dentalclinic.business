﻿
namespace DentalClinic.Application.Models
{
    //[VisitPatientRequire]
    public class VisitSimpleDto
    {
        public Guid? Id { get; set; }
        public string? SimplePeriod { get; set; }
        public Guid? DoctorId { get; set; } = Guid.Empty;
        public Guid? PatientId { get; set; } = Guid.Empty;
        public string DateVisit { get; set; }

        
        public string? Comment { get;set; }

        public NewPatientDto? NewPatient { get; set; }
    }
}
