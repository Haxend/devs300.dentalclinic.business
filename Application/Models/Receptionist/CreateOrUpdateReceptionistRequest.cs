﻿namespace DentalClinic.Application.Models.Raceptionist
{
    public class CreateOrUpdateReceptionistRequest
    {
        public string? FirstName { get; set; }

        public string? FatherName { get; set; }

        public string LastName { get; set; } = null!;

        public string? Email { get; set; }

        public string? PasswordHash { get; set; }

        public string? PhoneNumber { get; set; }

        public DateTime? CreatedAt { get; set; }
    }
}
