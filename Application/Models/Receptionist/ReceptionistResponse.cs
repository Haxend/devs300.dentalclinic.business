﻿namespace DentalClinic.Application.Models.Raceptionist
{
    public class ReceptionistResponse
    {
        public Guid Id { get; set; }

        public string? FirstName { get; set; }

        public string? FatherName { get; set; }

        public string LastName { get; set; } = null!;

        public string? PhoneNumber { get; set; }

        public string? Email { get; set; }

        public DateTime? CreatedAt { get; set; }
    }
}
