﻿using DentalClinic.Application.Models.Base;

namespace DentalClinic.Application.Models.Person.Response
{
    public class PersonUpdateResponse : BaseResponse
    {
        public Guid Id { get; set; }
    }
}
