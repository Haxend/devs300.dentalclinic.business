﻿using DentalClinic.Application.Models.Base;

namespace DentalClinic.Application.Models.Person.Response
{
    public class PersonCreateResponse : BaseResponse
    {
        public Guid Id { get; set; }
    }
}
