﻿using DentalClinic.Application.Models.Base;

namespace DentalClinic.Application.Models.Person.Response
{
    public class PersonDeleteResponse : BaseResponse
    {
        public Guid Id { get; set; }
    }
}
