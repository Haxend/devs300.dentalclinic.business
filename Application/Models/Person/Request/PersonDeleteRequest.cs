﻿

namespace DentalClinic.Application.Models.Person.Request
{
    public class PersonDeleteRequest
    {
        public Guid Id { get; set; }
    }
}
