﻿

namespace DentalClinic.Application.Models.Person.Request
{
    public class PersonGetRequest
    {
        public Guid Id { get; set; }
    }
}
