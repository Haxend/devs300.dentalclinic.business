﻿
namespace DentalClinic.Application.Models.Person.Request
{
    public class PersonUpdateRequest
    {
        public Guid Id { get; set; }

        /// <summary>
        /// Возвращает, задает имя пользователя
        /// </summary>
        public string? FirstName { get; set; }

        /// <summary>
        /// Возвращает, задает отчество пользователя
        /// </summary>
        public string? FatherName { get; set; }

        /// <summary>
        /// Возвращает, задает фамилию пользователя
        /// </summary>
        public string LastName { get; set; } = null!;

        /// <summary>
        /// Возвращает, задает почту пользователя
        /// </summary>
        public string? Email { get; set; }

        /// <summary>
        /// Возвращает, задает хэш-пароль пользователя
        /// </summary>
        public string? PasswordHash { get; set; }

        /// <summary>
        /// Возвращает, задает телефонный номер пользователя
        /// </summary>
        public string? PhoneNumber { get; set; }       
    }
}
