﻿using AutoMapper;

namespace DentalClinic.Application.Models.Person
{
    public class PersonDto
    {
        public Guid Id { get; set; }

        /// <summary>
        /// Возвращает, задает имя пользователя
        /// </summary>
        public string? FirstName { get; set; }

        /// <summary>
        /// Возвращает, задает отчество пользователя
        /// </summary>
        public string? FatherName { get; set; }

        /// <summary>
        /// Возвращает, задает фамилию пользователя
        /// </summary>
        public string LastName { get; set; } = null!;

        /// <summary>
        /// Возвращает, задает почту пользователя
        /// </summary>
        public string? Email { get; set; }

        /// <summary>
        /// Возвращает, задает хэш-пароль пользователя
        /// </summary>
        public string? PasswordHash { get; set; }

        /// <summary>
        /// Возвращает, задает телефонный номер пользователя
        /// </summary>
        public string? PhoneNumber { get; set; }

        /// <summary>
        /// Возвращает, задает дату создания пользователя
        /// </summary>
        public DateTime? CreatedAt { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<PersonDto, DentailClinic.Domain.Entities.Person>()
                .ForMember(entity => entity.FirstName,
                    opt => opt.MapFrom(dto => dto.FirstName))
                .ForMember(entity => entity.LastName,
                    opt => opt.MapFrom(dto => dto.LastName))
                .ForMember(entity => entity.FatherName,
                    opt => opt.MapFrom(dto => dto.FatherName))
                .ForMember(entity => entity.PhoneNumber,
                    opt => opt.MapFrom(dto => dto.PhoneNumber))
                .ForMember(entity => entity.Email,
                    opt => opt.MapFrom(dto => dto.Email))
                .ForMember(entity => entity.PasswordHash,
                    opt => opt.MapFrom(dto => dto.PasswordHash))
                .ForMember(entity => entity.CreatedAt,
                    opt => opt.MapFrom(dto => dto.CreatedAt));
        }
    }
}
