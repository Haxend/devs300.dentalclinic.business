﻿using AutoMapper;
using DentalClinic.Business.Domain.Entities;
using DentalClinic.Common.Contracts.ServicePrice;

namespace DentalClinic.Business.Application.Mappings
{
    public class ServicePriceMapper : Profile
    {
        public ServicePriceMapper() 
        {
            // Create
            //CreateMap<ServicePriceCreateRequest, ServicePrice>();
            //CreateMap<ServicePrice, ServicePriceCreateResponse>();

            // Delete
            //CreateMap<ServicePriceDeleteRequest, ServicePrice>();
            //CreateMap<ServicePrice, ServicePriceDeleteResponse>();

            // Update
            //CreateMap<ServicePriceUpdateRequest, ServicePrice>();
            //CreateMap<ServicePrice, ServicePriceUpdateResponse>();

            // Get
            //CreateMap<ServicePrice, ServicePriceGetRequest>();
            //CreateMap<ServicePrice, ServicePriceGetResponse>();
        }
    }
}
