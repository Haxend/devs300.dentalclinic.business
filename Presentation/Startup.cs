﻿using DentalClinic.Business.Application;
using DentalClinic.Business.Persistence;
using MassTransit;
using Microsoft.OpenApi.Models;

namespace DentalClinic.Business.Presentation
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.ConfigurePersistence(Configuration);
            services.ConfigureApplication(Configuration);
            services.AddConfigurations(Configuration);

            services.ConfigureApiBehavior();
            services.ConfigureCorsPolicy();

            services.AddMvc();

            services.AddControllers();
            services.AddEndpointsApiExplorer();

            services.AddMassTransit(x =>
            {
                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.Host(Configuration["RabbitMQ:Host"], h =>
                    {
                        h.Username(Configuration["RabbitMQ:Username"]);
                        h.Password(Configuration["RabbitMQ:Password"]);
                    });
                    cfg.ConfigureEndpoints(context);
                });

                // Разкомментить при реализации консюмеров

                // OnlineAppointment
                //x.AddConsumer<CreateOnlineAppointmentConsumer>();
                //x.AddConsumer<GetOnlineAppointmentConsumer>();
                //x.AddConsumer<GetOnlineAppointmentsListConsumers>();
                //x.AddConsumer<DeleteOnlineAppointmentConsumer>();
                //x.AddConsumer<ApproveOnlineAppointmentConsumer>();

                //Schedule
                //x.AddConsumer<GetSimpleByPeriodScheduleConsumer>();
                //x.AddConsumer<GetByPeriodScheduleConsumer>();
                //x.AddConsumer<GenerateForPeriodScheduleConsumers>();
                //x.AddConsumer<MakeAppointmentExistedPatientExistedSlotScheduleConsumer>();
                //x.AddConsumer<MakeAppointmentNewPatientNewSlotScheduleConsumer>();

                //ServicePrice
                //x.AddConsumer<CreateServicePriceConsumer>();
                //x.AddConsumer<GetServicePriceConsumer>();
                //x.AddConsumer<GetServicePricesListConsumers>();
                //x.AddConsumer<UpdateServicePriceConsumer>();
                //x.AddConsumer<DeleteServicePriceConsumer>();

                //Visit
                //x.AddConsumer<CreateServicePriceConsumer>();
                //x.AddConsumer<GetServicePriceConsumer>();
                //x.AddConsumer<GetServicePricesListConsumers>();
                //x.AddConsumer<UpdateServicePriceConsumer>();
                //x.AddConsumer<DeleteServicePriceConsumer>();

                //Procedure
                //x.AddConsumer<CreateServicePriceConsumer>();
                //x.AddConsumer<GetServicePriceConsumer>();
                //x.AddConsumer<GetServicePricesListConsumers>();
                //x.AddConsumer<UpdateServicePriceConsumer>();
                //x.AddConsumer<DeleteServicePriceConsumer>();
            });

            services.AddSwaggerGen(option =>
            {
                option.SwaggerDoc("v1", new OpenApiInfo { Title = "DenatalClinic.Business API", Version = "v1" });
            });


        }

        public void Configure(WebApplication app, IWebHostEnvironment env)
        {
            app.UseCors();
            app.MapControllers();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseStaticFiles();
            app.UseHttpsRedirection();
            app.UseRouting();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "DenatalClinic.Business API v1");
            });
        }
    }
}
