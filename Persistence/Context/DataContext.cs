﻿using DentalClinic.Business.Domain.Entities;
using DentalClinic.Business.Domain.Enum;
using Microsoft.EntityFrameworkCore;

namespace DentalClinic.Business.Persistence.Context
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<OnlineAppointment> OnlineAppointments { get; set; } = null!;
        public DbSet<ServicePrice> ServicePrices { get; set; } = null!;
        public DbSet<Visit> Visits { get; set; } = null!;
        public DbSet<VisitProcedure> VisitProcedures { get; set; } = null!;
        public DbSet<Tooth> Teeth { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            FillDefaultData(modelBuilder);
        }

        private void FillDefaultData(ModelBuilder builder)
        {
            builder.Entity<Tooth>().HasData([
            new Tooth { Id = Guid.NewGuid(), Code = 11, Name = "верхний правый первый резец" },
            new Tooth { Id = Guid.NewGuid(), Code = 12, Name = "верхний правый второй резец" },
            new Tooth { Id = Guid.NewGuid(), Code = 13, Name = "верхний правый клык" },
            new Tooth { Id = Guid.NewGuid(), Code = 14, Name = "верхний правый первый премоляр" },
            new Tooth { Id = Guid.NewGuid(), Code = 15, Name = "верхний правый второй премоляр" },
            new Tooth { Id = Guid.NewGuid(), Code = 16, Name = "верхний правый первый моляр" },
            new Tooth { Id = Guid.NewGuid(), Code = 17, Name = "верхний правый второй моляр" },
            new Tooth { Id = Guid.NewGuid(), Code = 18, Name = "верхний правый третий моляр" },

            new Tooth { Id = Guid.NewGuid(), Code = 21, Name = "верхний левый первый резец" },
            new Tooth { Id = Guid.NewGuid(), Code = 22, Name = "верхний левый второй резец" },
            new Tooth { Id = Guid.NewGuid(), Code = 23, Name = "верхний левый клык" },
            new Tooth { Id = Guid.NewGuid(), Code = 24, Name = "верхний левый первый премоляр" },
            new Tooth { Id = Guid.NewGuid(), Code = 25, Name = "верхний левый второй премоляр" },
            new Tooth { Id = Guid.NewGuid(), Code = 26, Name = "верхний левый первый моляр" },
            new Tooth { Id = Guid.NewGuid(), Code = 27, Name = "верхний левый второй моляр" },
            new Tooth { Id = Guid.NewGuid(), Code = 28, Name = "верхний левый третий моляр" },

            new Tooth { Id = Guid.NewGuid(), Code = 31, Name = "нижний правый первый резец" },
            new Tooth { Id = Guid.NewGuid(), Code = 32, Name = "нижний правый второй резец" },
            new Tooth { Id = Guid.NewGuid(), Code = 33, Name = "нижний правый клык" },
            new Tooth { Id = Guid.NewGuid(), Code = 34, Name = "нижний правый первый премоляр" },
            new Tooth { Id = Guid.NewGuid(), Code = 35, Name = "нижний правый второй премоляр" },
            new Tooth { Id = Guid.NewGuid(), Code = 36, Name = "нижний правый первый моляр" },
            new Tooth { Id = Guid.NewGuid(), Code = 37, Name = "нижний правый второй моляр" },
            new Tooth { Id = Guid.NewGuid(), Code = 38, Name = "нижний правый третий моляр" },

            new Tooth { Id = Guid.NewGuid(), Code = 41, Name = "нижний левый первый резец" },
            new Tooth { Id = Guid.NewGuid(), Code = 42, Name = "нижний левый второй резец" },
            new Tooth { Id = Guid.NewGuid(), Code = 43, Name = "нижний левый клык" },
            new Tooth { Id = Guid.NewGuid(), Code = 44, Name = "нижний левый первый премоляр" },
            new Tooth { Id = Guid.NewGuid(), Code = 45, Name = "нижний левый второй премоляр" },
            new Tooth { Id = Guid.NewGuid(), Code = 46, Name = "нижний левый первый моляр" },
            new Tooth { Id = Guid.NewGuid(), Code = 47, Name = "нижний левый второй моляр" },
            new Tooth { Id = Guid.NewGuid(), Code = 48, Name = "нижний левый третий моляр" },
            ]);

            builder.Entity<OnlineAppointment>().HasData([
            new OnlineAppointment()
            {
                Id = Guid.NewGuid(),
                Approved = false,
                Description = "",
                PhoneNumber = "+79228888887",
                Email = "none@mail.ru",
                WantedDateVisit = new DateTime(2024,2,14).ToUniversalTime(),
            },
            new OnlineAppointment()
            {
                Id = Guid.NewGuid(),
                Approved = false,
                Description = "",
                PhoneNumber = "+79229999999",
                Email = "none@mail.ru",
                WantedDateVisit = new DateTime(2024,2,12).ToUniversalTime(),
            }]);

            builder.Entity<ServicePrice>().HasData([
            new ServicePrice
            {
                Id = Guid.NewGuid(),
                Code = "В01.065.003",
                Name = "Приём (осмотр, консультация) зубного врача первичный",
                Price = 500
            },
            new ServicePrice
            {
                Id = Guid.NewGuid(),
                Code = "В01.065.004",
                Name = "Приём (осмотр, консультация) зубного врача повторный",
                Price = 300
            },
            new ServicePrice
            {
                Id = Guid.NewGuid(),
                Code = "В01.003.004.005",
                Name = "Анестезия",
                Price = 500
            },
            new ServicePrice
            {
                Id = Guid.NewGuid(),
                Code = "А16.07.051",
                Name = "Профессиональная гигиена полости рта и зубов",
                Price = 3500
            },
            new ServicePrice
            {
                Id = Guid.NewGuid(),
                Code = "А16.07.002.008",
                Name = "Восстановление стенки зуба с использованием фотополимеров, матричных систем",
                Price = 1200
            },
            new ServicePrice
            {
                Id = Guid.NewGuid(),
                Code = "А16.07.002.010",
                Name = "Удаление зуба",
                Price = 5200
            }]);

            builder.Entity<Visit>().HasData([
            new Visit
            {
                Id = Guid.NewGuid(),
                DateTimeBegin = new DateTime(2024,1,10,10,0,0).ToUniversalTime(),
                DateTimeEnd = new DateTime(2024,1,10,11,0,0).ToUniversalTime(),
                DoctorID = null,
                Comment = "",
                DateOfVisit = new DateTime(2024,1,10,11,0,0).ToUniversalTime(),
                PatientID = null,
                Status = StatusOfVisit.Registered,
                FromOnline = true,
                Procedures = new List<VisitProcedure>
                {
                    new VisitProcedure
                    {
                        Id = Guid.NewGuid(),
                        Service = ServicePrices.First(p=>p.Code=="В01.065.003"),
                        CountOfThisProcedures = 1,
                        Sum = 500
                    },
                    new VisitProcedure
                    {
                        Id = Guid.NewGuid(),
                        Service = ServicePrices.First(p=>p.Code=="А16.07.002.008"),
                        CountOfThisProcedures = 3,
                        Sum = 3600,
                        //Tooth = Teeth[10]
                    },
                }
            },
            new Visit
            {
                Id = Guid.NewGuid(),
                DateTimeBegin = new DateTime(2024,1,10,13,0,0).ToUniversalTime(),
                DateTimeEnd = new DateTime(2024,1,10,14,0,0).ToUniversalTime(),
                Comment = "",
                DateOfVisit = new DateTime(2024,1,10,11,0,0).ToUniversalTime(),
                DoctorID = null,
                PatientID = null,
                Status = StatusOfVisit.Success,
                FromOnline = false,
                Procedures = new List<VisitProcedure>
                {
                    new VisitProcedure
                    {
                        Id = Guid.NewGuid(),
                        Service = ServicePrices.First(p=>p.Code=="В01.065.004"),
                        CountOfThisProcedures = 1,
                        Sum = 500
                    },
                    new VisitProcedure
                    {
                        Id = Guid.NewGuid(),
                        Service = ServicePrices.First(p=>p.Code=="В01.003.004.005"),
                        CountOfThisProcedures = 2,
                        Sum = 1000
                    },
                    new VisitProcedure
                    {
                        Id = Guid.NewGuid(),
                        Service = ServicePrices.First(p=>p.Code=="А16.07.051"),
                        CountOfThisProcedures = 1,
                        Sum = 3500
                    },
                }
            },
            new Visit
            {
                Id = Guid.NewGuid(),
                DateTimeBegin = new DateTime(2024,1,12,14,0,0).ToUniversalTime(),
                DateTimeEnd = new DateTime(2024,1,12,15,0,0).ToUniversalTime(),
                Comment = "Комментарий",
                DoctorID = null,
                PatientID = null,
                DateOfVisit = new DateTime(2024,1,12,14,0,0).ToUniversalTime(),
                Status = StatusOfVisit.Success,
                FromOnline = false,
                Procedures = new List<VisitProcedure>
                {
                    new VisitProcedure
                    {
                        Id = Guid.NewGuid(),
                        Service = ServicePrices.First(p=>p.Code=="А16.07.002.010"),
                        CountOfThisProcedures = 1,
                        Sum = 5200,
                        //Tooth = Teeth[13]
                    }
                }
            },
            new Visit
            {
                Id = Guid.NewGuid(),
                DateTimeBegin = new DateTime(2024,1,11,10,0,0).ToUniversalTime(),
                DateTimeEnd = new DateTime(2024,1,11,11,0,0).ToUniversalTime(),
                DoctorID = null,
                PatientID = null,
                Status = StatusOfVisit.Registered,
                FromOnline = false,
            },
            new Visit
            {
                Id = Guid.NewGuid(),
                DateTimeBegin = new DateTime(2024,1,11,11,0,0).ToUniversalTime(),
                DateTimeEnd = new DateTime(2024,1,11,12,0,0).ToUniversalTime(),
                DoctorID = null,
                PatientID = null,
                Status = StatusOfVisit.Registered,
                FromOnline = false,
            }]);
        }
    }
}
