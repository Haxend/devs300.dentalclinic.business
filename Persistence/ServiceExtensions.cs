﻿using DentalClinic.Business.Application.Interfaces;
using DentalClinic.Business.Infrastructure.Settings;
using DentalClinic.Business.Persistence.Context;
using DentalClinic.Business.Persistence.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DentalClinic.Business.Persistence
{
    public static class ServiceExtensions
    {
        public static void ConfigurePersistence(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("PostgreSQL");
            services.AddDbContext<DataContext>(opt => { opt.UseNpgsql(connectionString); });

            //Заранее создал класс настроек проекта ApplicationSettings
            var applicationSettings = configuration.Get<ApplicationSettings>();

            services.AddSingleton(applicationSettings)
                    .AddSingleton((IConfigurationRoot)configuration)
                    .InstallServices();
        }

        public static IServiceCollection InstallServices(this IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork, UnitOfWork<DataContext>>();
            //services.AddTransient<IBaseRepository<User>, BaseRepository<User>>();
            return services;
        }
    }
}
