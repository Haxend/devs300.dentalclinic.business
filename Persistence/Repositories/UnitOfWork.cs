﻿using DentalClinic.Business.Application.Interfaces;
using DentalClinic.Business.Domain;
using DentalClinic.Business.Persistence.Context;

namespace DentalClinic.Business.Persistence.Repositories
{
    public class UnitOfWork<TContext> : IUnitOfWork where TContext : DataContext
    {
    
        private readonly TContext context;
        private readonly Dictionary<Type,object> repositories = new Dictionary<Type,object>();

        public UnitOfWork(TContext context)
        {
            this.context = context;
        }

        public IBaseRepository<T> GetRepository<T>() where T : BaseEntity
        {
            var type = typeof(T);
            if(!repositories.ContainsKey(type))
            {
                repositories[type] = new BaseRepository<T>(context);
            }
            return (IBaseRepository<T>)repositories[type];
        }

        public int SaveChanges()
        {
            return context.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await context.SaveChangesAsync();
        }
    }
}
