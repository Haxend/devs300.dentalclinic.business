﻿using DentalClinic.Business.Application.Interfaces;
using DentalClinic.Business.Domain;
using DentalClinic.Business.Persistence.Context;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using System.Threading;

namespace DentalClinic.Business.Persistence.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        private readonly DataContext Context;

        public BaseRepository(DataContext context)
        {
            Context = context;
        }

        public async Task<Guid> CreateAsync(T entity)
        {
            entity.CreatedAt = DateTimeOffset.UtcNow;
            await Context.AddAsync(entity);
            await Context.SaveChangesAsync();
            return entity.Id;
        }

        public async Task UpdateAsync(T entity)
        {
            entity.UpdatedAt = DateTimeOffset.UtcNow;
            Context.Update(entity);
            await Context.SaveChangesAsync();
        }

        public async Task DeleteAsync(T entity)
        {
            Context.Remove(entity);
            await Context.SaveChangesAsync();
        }

        public async Task<T?> GetByIdAsync(Guid id)
        {
            return await Context.Set<T>().SingleOrDefaultAsync(x => x.Id == id);
        } 

        public async Task<bool> HasAnyByIdAsync(Guid id)
        {
            return await Context.Set<T>().AnyAsync(x => x.Id == id);
        }

        async Task<IEnumerable<T>> IBaseRepository<T>.GetAllAsync()
        {
            return await Context.Set<T>().ToListAsync();
        }
    }
}
